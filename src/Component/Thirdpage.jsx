import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default function Thirdpage() {
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <div className="bg-light p-3 h-100 mt-4">
              <h1 className="fs-3 fw-bold text-center">Vision</h1>
              <ul>
                <li>
                  To be the best SW Professional Training Center in Cambodia
                </li>
              </ul>
            </div>
          </Col>
          <Col>
            <div className="bg-light p-3 h-100 mt-4">
              <p className="fs-3 fw-bold text-center">Mission</p>
              <ul>
                <li>High quality training and research</li>
                <li>
                  Developing Capacity of SW Experts to be Leaders in IT Field
                </li>
                <li>Developing sustainable ICT Program</li>
              </ul>
            </div>
          </Col>
        </Row>

        <Row className="my-3">
          <Col>
            <div className="bg-light p-3 h-100 mt-4">
              <h1 className="fs-3 fw-bold text-center">Strategy</h1>
              <ul>
                <li>
                  Best training method with up to date curriculum and
                  environment
                </li>
                <li>
                  Cooperation with the best IT industry to guarantee student's
                  career and benefits
                </li>
                <li>Additional Soft Skill, Management, Leadership training</li>
              </ul>
            </div>
          </Col>
          <Col>
            <div className="bg-light p-3 h-100 mt-4">
              <p className="fs-3 fw-bold text-center">Slogan</p>
              <ul>
                <li>
                  "KSHRD, connects you to various opportunities in IT Field."
                </li>
                <li>
                  Raising brand awareness with continuous advertisement of SNS
                  and any other media
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
