import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, Container, Row } from "react-bootstrap";

export default function Secondpage() {
  return (
    <div>
      <Container>
        <Row>
          <div className="col-6 mt-4 ">
            <Card.Img
              variant="bottom"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              style={{
                width: "100%",
                height: "100%",
                display: "flex",
                margin: 5,
              }}
            />
          </div>

          <div className="col-6 mt-4">
            <Card body style={{ height: "100%" }}>
              {" "}
              <p>
                {" "}
                Korea Software HRD Center is an academy training center for
                training software professionals in cooperation with Korea
                International Cooperation Agency(KOICA) and Webcash in April,
                2013 in Phnom Penh, Cambodia. <br />
                <br /> From 2020, Korea Software HRD Center has been become
                Global NGO with the name Foundation for Korea Software Global
                Aid (KSGA), main sponsored by Webcash Group, to continue mission
                for ICT Development in Cambodia and will recruit 60 t0 80
                scholarship students every year.
              </p>
            </Card>
          </div>
        </Row>

        <Row>
          <div className="col-6 mt-4">
            <Card body style={{ height: "100%" }}>
              {" "}
              <p>
                {" "}
                Korea Software HRD Center is an academy training center for
                training software professionals in cooperation with Korea
                International Cooperation Agency(KOICA) and Webcash in April,
                2013 in Phnom Penh, Cambodia. <br />
                <br /> From 2020, Korea Software HRD Center has been become
                Global NGO with the name Foundation for Korea Software Global
                Aid (KSGA), main sponsored by Webcash Group, to continue mission
                for ICT Development in Cambodia and will recruit 60 t0 80
                scholarship students every year.
              </p>
            </Card>
          </div>

          <div className="col-6 mt-4">
            <Card.Img
              variant="bottom"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              style={{
                width: "100%",
                height: "100%",
                display: "flex",
                margin: 5,
              }}
            />
          </div>
        </Row>

        <Row>
          <div className="col-6 mt-4">
            <Card.Img
              variant="bottom"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
              style={{
                width: "100%",
                height: "100%",
                display: "flex",
                margin: 5,
              }}
            />
          </div>

          <div className="col-6 mt-4">
            <Card body style={{ height: "100%" }}>
              {" "}
              <p>
                {" "}
                Korea Software HRD Center is an academy training center for
                training software professionals in cooperation with Korea
                International Cooperation Agency(KOICA) and Webcash in April,
                2013 in Phnom Penh, Cambodia. <br />
                <br /> From 2020, Korea Software HRD Center has been become
                Global NGO with the name Foundation for Korea Software Global
                Aid (KSGA), main sponsored by Webcash Group, to continue mission
                for ICT Development in Cambodia and will recruit 60 t0 80
                scholarship students every year.
              </p>
            </Card>
          </div>
        </Row>
      </Container>
    </div>
  );
}
