import "./App.css";
import Firstpage from "./Component/Firstpage";
import Secondpage from "./Component/Secondpage";
import Thirdpage from "./Component/Thirdpage";
import { Route, Routes } from "react-router-dom";
import NavComponent from "./Component/NavComponent";
import FooterComponent from "./Component/FooterComponent";

function App() {
  return (
    <div className="App">
      <NavComponent />
      <Routes>
        {/* <Route path='/' element={<NavComponent/>}></Route> */}
        <Route path="/" element={<Firstpage />}></Route>
        <Route path="/About" element={<Secondpage />}></Route>
        <Route path="/OurVision" element={<Thirdpage />}></Route>
      </Routes>

      <FooterComponent />
    </div>
  );
}

export default App;
